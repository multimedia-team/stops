stops (0.4.0-1) unstable; urgency=medium

  * New upstream version 0.4.0
  * Add me as uploader
  * Bump dh-compat to 13
  * Bump Standards-Version to 4.6.2
  * Set RRR: no
  * Remove obsolete d/stops.dirs
  * Fix d/watch download url

 -- Dennis Braun <snd@debian.org>  Tue, 10 Jan 2023 14:35:31 +0100

stops (0.3.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Team upload.
  * Use debhelper-compat instead of debian/compat

  [ Olivier Humbert ]
  * Update d/control, re: #946641
  * d/control: http -> https
  * d/copyright: http -> https
  * d/copyright: adds upstream-name
  * d/watch: http -> https
  * d/copyright: add myself

  [ Sebastian Ramacher ]
  * debian/control:
    - Integrate changes to Description suggested by Daniel James
    - Bump Standards-Version
    - Bump debhelper compat to 12
    - Mark as M-A: foreign (Closes: #946641)

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 16 Dec 2019 20:08:34 +0100

stops (0.3.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild package. (Closes: #907293)

  [ Alessio Treglia ]
  * Update Maintainer field.
  * Switch to '3.0 (quilt)' packaging format.

  [ Ondřej Nový ]
  * d/control:
    - Deprecating priority extra as per policy 4.0.1.
    - Set Vcs-* to salsa.debian.org. (Closes: #888669)
  * d/rules: Remove trailing whitespaces

  [ James Cowgill ]
  * d/compat: Use debhelper compat 11
  * d/copyright: Convert to machine readable format.
  * d/control:
    - Add Homepage.
    - Drop ${shlibs:Depends}.
    - Bump standards to version 4.2.1.
    - Expand description based on aeolus package.
  * d/rules: Convert to dh.
  * d/watch: Update to new download location.

 -- James Cowgill <jcowgill@debian.org>  Sun, 02 Sep 2018 14:00:16 +0100

stops (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #420589)

 -- Free Ekanayaka <freee@debian.org>  Mon, 23 Apr 2007 14:06:08 +0200
